import rospy
import numpy
from geometry_msgs.msg import Twist
from sensor_msgs.msg import LaserScan
from p2os_msgs.msg import SonarArray

def callback4Sonar(sensor_data):
	base_data = Twist()
#	mindis=min(sensor_data.ranges)
	LFSonar=min(sensor_data.ranges[1:3])
	RFSonar=min(sensor_data.ranges[4:6])
	if LFSonar<0.1:
		base_data.angular.z=-0.5
	elif RFSonar<0.1:
		base_data.angular.z=0.5
	else:
		base_data.linear.x=0.1
	pub.publish( base_data)

def callback4Laser(sensor_data):
	base_data = Twist()
#	mindis=min(sensor_data.ranges)
	LF=min(sensor_data.ranges[171:255])
	RF=min(sensor_data.ranges[256:340])
	if LF<0.5 and RF<0.5:
		if LF>RF: 
			base_data.angular.z=0.4
			base_data.linear.x=0.1
		else:
			base_data.angular.z=-0.4
			base_data.linear.x=0.1
		#base_data.linear.x=-0.4
		#base_data.angular.z=0.3
	elif LF<0.1 and RF>0.1 :
		base_data.angular.z=-0.5
		base_data.linear.x=0.1
	elif RF<0.1 and LF>0.1:
		base_data.angular.z=0.5
		base_data.linear.x=0.1
	else:
		base_data.linear.x=0.1
	pub.publish( base_data)

def callback(sensor_data):
	base_data = Twist()	
	base_data.angular.z=0.05
	pub.publish( base_data)

if __name__ == '__main__':
	rospy.init_node('reactive_mover_node')
	rospy.Subscriber('base_scan', LaserScan, callback)
	rospy.Subscriber('base_scan', LaserScan, callback4Laser)
	rospy.Subscriber('/sonar', SonarArray, callback4Sonar)
	pub = rospy.Publisher('cmd_vel', Twist, queue_size=100)
	rospy.spin()

