#! /usr/bin/env python
# This basic node subscribes to incoming laser and sonar messages,
# decides what to do based on the incoming data, then publishes
# movement commands to the robot.

# Remember to comment your code so your team mates can understand it!

# @author Claudio Zito, Marco Becerra
import rospy
import math
import numpy
from geometry_msgs.msg import Twist
from sensor_msgs.msg import LaserScan
from p2os_msgs.msg import SonarArray
LEFT_SINGLE_SONAR_PREVIOUS =0
RIGHT_SINGLE_SONAR_PREVIOUS =0

def callback4Sonar(sensor_data):
	global LEFT_SINGLE_SONAR_PREVIOUS
	global RIGHT_SINGLE_SONAR_PREVIOUS
	THRESHOLD = 0.2
	base_data = Twist()
	LFSonar=min(sensor_data.ranges[0:3])
	LEFT_SINGLE_SONAR=sensor_data.ranges[0]
	RIGHT_SINGLE_SONAR=sensor_data.ranges[7]
	print("LEFT PREVIOUS:", LEFT_SINGLE_SONAR_PREVIOUS)
	print("LEFT CURRENT", LEFT_SINGLE_SONAR)
	print("RIGHT PREVIOUS:", RIGHT_SINGLE_SONAR_PREVIOUS)
	print("RIGHT CURRENT", RIGHT_SINGLE_SONAR)
	RFSonar=min(sensor_data.ranges[4:7])
	if LFSonar<1 and RFSonar>1 :
		base_data.angular.z=-0.8
		print("OBSTACLE ON LEFT SIDE:", min(sensor_data.ranges[1:3]))
	elif RFSonar<1 and LFSonar>1 :
		base_data.angular.z=0.8
		print("OBSTACLE ON RIGHT SIDE:", min(sensor_data.ranges[4:6]))
	elif (LFSonar<1 and RFSonar<1):
		min_value_index=sensor_data.ranges.index(min(sensor_data.ranges[1:6]))
		print(min_value_index)
		if(min_value_index > 0 and min_value_index <4):
			base_data.angular.z=-1.2
			print("OBSTACLE IN FRONT BUT LEFT ")
		elif (min_value_index >3 and min_value_index <7):
			base_data.angular.z=1.2
			print("OBSTACLE IN FRONT BUT RIGHT")
	
	else:
		print("NO OBSTACLES")
		if  (LFSonar>0.5 and RFSonar>0.5):
			base_data.linear.x=0.8
		else: 
			base_data.linear.x=0
			base_data.angular.z=3.0
			print("STOPPING")
		print(base_data)
	LEFT_SINGLE_SONAR_PREVIOUS = LEFT_SINGLE_SONAR
	RIGHT_SINGLE_SONAR_PREVIOUS = RIGHT_SINGLE_SONAR
	pub.publish( base_data)

if __name__ == '__main__':
    rospy.init_node('reactive_mover_node')
    #rospy.Subscriber('base_scan', LaserScan, callback)
    #rospy.Subscriber('base_scan', LaserScan, callback4Laser)
    rospy.Subscriber('/sonar', SonarArray, callback4Sonar)
    pub = rospy.Publisher('cmd_vel', Twist, queue_size=100)
    rospy.spin()
