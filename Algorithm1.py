#! /usr/bin/env python

# The proposed idea is that if an obstacle is on the left (right) side of the robot, and thus the value on the sensor will be lower than a threshold, minDis,
# LSONAR (RSONAR) will be assigned this minimum value and the robot will rotate clockwise(anticlockwise).
# If an obstacle is detected in front of the robot, the algorithm checks which sensor is closest to the obstacle and rotates at a large angle value in the opposite direction.
# Provided that no obstacles are detected, the robot traverses in the forward direction. 

# Team Stuart

import rospy
import math
import numpy
from geometry_msgs.msg import Twist
from p2os_msgs.msg import SonarArray
LEFT_SINGLE_SONAR_PREVIOUS =0
RIGHT_SINGLE_SONAR_PREVIOUS =0

def callback4Sonar(sensor_data):
	base_data = Twist()
	#separate sensor values into left and right
	LSonar=min(sensor_data.ranges[0:3])
	RSonar=min(sensor_data.ranges[4:7])
	minDis=1;
	#conditions to check where the obstacle is
	if LSonar<minDis and RSonar>minDis :
		base_data.angular.z=-0.8
		print("OBSTACLE ON LEFT SIDE:", min(sensor_data.ranges[0:3]))
	elif RSonar<minDis and LSonar>minDis :
		base_data.angular.z=0.8
		print("OBSTACLE ON RIGHT SIDE:", min(sensor_data.ranges[4:7]))
	#check if obstacle is in front of robot
	elif (LSonar<minDis and RSonar<minDis):
		min_value_index=sensor_data.ranges.index(min(sensor_data.ranges[1:6]))
		print(min_value_index)
		#obstacle is in front...therefore rotate a larger angle
		if(min_value_index > 0 and min_value_index <4):
			#obstacle is in front...therefore rotate a larger angle
			base_data.angular.z=-1.2
			print("OBSTACLE IN FRONT BUT MORE TO THE LEFT HAND SIDE")
		elif (min_value_index >3 and min_value_index <7):
			base_data.angular.z=1.2
			print("OBSTACLE IN FRONT BUT MORE TO THE RIGHT HAND SIDE")
	else:
		print("NO OBSTACLES")
		#ensure that no obstacles are very close to the robot
		if  (LSonar>0.5 and RSonar>0.5):
			base_data.linear.x=0.8
		else: 
			base_data.linear.x=0
			base_data.angular.z=3.0
		print(base_data)
	pub.publish(base_data)

if __name__ == '__main__':
    rospy.init_node('reactive_mover_node')
    rospy.Subscriber('/sonar', SonarArray, callback4Sonar)
    pub = rospy.Publisher('cmd_vel', Twist, queue_size=100)
    rospy.spin()
