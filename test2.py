#! /usr/bin/env python
# This basic node subscribes to incoming laser and sonar messages,
# decides what to do based on the incoming data, then publishes
# movement commands to the robot.

# Remember to comment your code so your team mates can understand it!

# @author Claudio Zito, Marco Becerra
import rospy
import math
import numpy
from geometry_msgs.msg import Twist
from sensor_msgs.msg import LaserScan
from p2os_msgs.msg import SonarArray

def callback4Laser(sensor_data):
	#sensor_data (LaserScan data type) has the laser scanner data
	#base_data (Twist data type) created to control the base robot
	base_data = Twist()
        print (numpy.min(sensor_data.ranges[0:511]))
        if (numpy.min(sensor_data.ranges) <1) or not math.isnan(numpy.min(sensor_data.ranges)):
                if numpy.min(sensor_data.ranges[0:255])  > numpy.min(sensor_data.ranges[256:511]):
                    base_data.angular.z =-0.8
                    print("aaa")
                else:
                    base_data.angular.z =0.8
                    print("bbb")
    
        else: 
                base_data.linear.x =0.1
                print(base_data)
        
	pub.publish(base_data)

def callback4Sonar(sensor_data):
	base_data = Twist()
#	mindis=min(sensor_data.ranges)
	LFSonar=min(sensor_data.ranges[1:3])
	RFSonar=min(sensor_data.ranges[4:6])
	if (sensor_data.ranges[0]) > LFSonar:
		base_data.angular.z=0.8
	else:
		if LFSonar<0.5:
			base_data.angular.z=-0.5
		elif RFSonar<0.5:
			base_data.angular.z=0.5
		else:
			base_data.linear.x=0.1
			print(base_data)
	
		
		pub.publish( base_data)




if __name__ == '__main__':
    rospy.init_node('reactive_mover_node')
    #rospy.Subscriber('base_scan', LaserScan, callback)
    #rospy.Subscriber('base_scan', LaserScan, callback4Laser)
    rospy.Subscriber('/sonar', SonarArray, callback4Sonar)
    pub = rospy.Publisher('cmd_vel', Twist, queue_size=100)
    rospy.spin()
