#! /usr/bin/env python
# This basic node subscribes to incoming laser and sonar messages,
# decides what to do based on the incoming data, then publishes
# movement commands to the robot.

# Remember to comment your code so your team mates can understand it!

# @author Claudio Zito, Marco Becerra
import rospy
import math
import numpy
import time
import string
from geometry_msgs.msg import Twist
from sensor_msgs.msg import LaserScan
from p2os_msgs.msg import SonarArray

## turning class: to record turning angle
class Turning:
	Angle=0


def Avg(range):
	return sum(range)/(len(range))
		

def Filter(range):
	list=[]
	for i in range:
		if i>0.001:
			list.append(i)
	return list


## filter data, change all the 'inf's and 'nan's to 30, because 30 is the longest distance the laser can detect
def Filter3(range):
	list=[]
	for i in range:
		
		if i==string.atof('inf'):
			list.append(5.6)
		elif i>0.001:
			list.append(i)	
		else:
			list.append(5.6)
	return list

def Filter2(range,Num):
	list=[]
	count=0
	for i in range:
		if i>0.001:	
			if count>0 and count<Num+1:
				for i in xrange(0,count):
					list.append(string.atof('inf'))
			elif count>Num:
				for i in xrange(0,count):
					list.append(0)
			count=0
			list.append(i)
		else:
			count=count+1
	return list

## the class to process and store all window data
class WinInfoList:
	List=[]
	## init function: 
	## input: laser data, window range 
	## description: this function accepts range(laser data) and minDis(window range). 
	## Then, according to the minDis, find the potential window, create WinInfo class to store each window data.
	## Finally, all the WinInfo classes which contain potential window data will be store into a list(List).
	def __init__(self,range,minDis):
		createFlag=False
		pInd=0
		self.List=[]
		for ind, dis in enumerate(range):
			#print(dis)
			if dis>minDis:
				if createFlag==False:
					wi=WinInfo(ind)
					createFlag=True
					pInd=ind
					self.List.append(wi)
			else:
				if createFlag==True:
					List_len=len(self.List)
					self.List[List_len-1].Last(ind,range[pInd:ind])	
					pInd=-1
					createFlag=False
			if ind==(len(range)-1) :
				lastInd=len(self.List)-1
				pInd=self.List[lastInd].Right
				if self.List[lastInd].Left==-1:
					self.List[lastInd].Last(ind,range[pInd:ind])
					createFlag=False		
		delList=[]
		for ind,item in enumerate(self.List):
			if item.Range<50:
				delList.insert(0,ind)
		for ind in delList:
			self.List.pop(ind)

	## Obstacleturn function
	## output: the minimum degree that the robot needs to turn.
	## description: used when the robot is faced with obstacles.
	## The function will find the window with the most window depth in the List, and calculate the minimum angle that the robot need to turn to go into the window.
	def Obstacleturn(self):
		maxWin=0
		maxInd=-1
		degree=0
		for ind,r in enumerate(self.List):
			
			if r.WinDepth>maxWin:
				maxWin=r.WinDepth
				maxInd=ind
		if(maxInd>-1):
			minPoint=self.List[maxInd].MidPoint
			left=self.List[maxInd].Left
			right=self.List[maxInd].Right
			if(minPoint>256):
				#left part
				degree=(right+95-255)/512.0*180.0
			
			elif(minPoint<256):
				degree=-((255-left+95)/512.0*180.0)
			else:
				degree=0 
			print('left: ',left)
			print('right: ',right)
		print(degree)
		return degree
	## Obstacleturn function
	## output: the middle point angle that the robot needs to turn.
	## description: used when the robot is moving straight forward.
	## The function will find the window with the most window depth in the List, and calculate the angle on the middle point that the robot need to turn to go into the window.
	def Straightturn(self):
		maxWin=0
		maxInd=-1
		degree=0
		for ind,r in enumerate(self.List):
			
			if r.WinDepth>maxWin:
				maxWin=r.WinDepth
				maxInd=ind 
		if(maxInd>-1):
			midPoint=self.List[maxInd].MidPoint
			left=self.List[maxInd].Left
			right=self.List[maxInd].Right
			if(midPoint==256):
				degree=0
			else:
				degree=(midPoint-256)/512.0*180.0
			print('left: ',left)
			print('right: ',right)
		print(degree)
		return degree
				
## the class to store window data
## description: 
## Left: stores the point index of the left side of the window
## Right: stores the point index of the right side of the window
## Range: stores the range of the window
## MidPoint: stores the middle point index of the window
## WinSTD: stores the standard diviation of the length of objects in the window
## WinDepth:= ((the average of the length of objects) - (the standard diviation of the length of objects)) * (the range of the window).
## The reason to use minus is that, it provide the shortest distance of the object might be in the window
class WinInfo:
	Left=-1
	Right=-1
	Range=-1
	MidPoint=-1
	WinSTD=-1
	WinDepth=-1
	def __init__(self,right):
		self.Right=right
	def Last(self,left,data):
		self.Left=left
		self.Range=self.Left-self.Right+1
		self.MidPoint=self.Right+self.Range/2
		baseLine=numpy.mean(data)
		std=numpy.std(data)
		self.WinSTD=std
		minLine=baseLine-std
		self.WinDepth=(baseLine-std)*self.Range

def callback4Laser(sensor_data):
	## get the twist data to base_data
	base_data = Twist() 
	## get the laser data in front of the robot
	Front=min(Filter3(sensor_data.ranges[169:340]))
	## set the minimum distance to 1
	minDis=1
	## change all the 'nan', 'inf' in the laser data into 30
	data=Filter3(sensor_data.ranges)
	## get the average of the laser data
	meandata=numpy.mean(data)
	## the basic logic is : if any object in front of the robot is less than the minimum distance, the robot will start to turn until the obstacle is not in the front it any more. 
	## Basically, the robot finds the potental window around it, and try to turn to that angle. But if no window is found, the robot turn to left by default.
	## When the robot is moving forward, it still calculates the window according to the average of laser data, and try to find the broader space in the surroundings.
	wil=WinInfoList(data,meandata)	
	if Front< minDis:
		if Turning.Angle==0:
			turningAng=wil.Obstacleturn()
			
			if (turningAng<0):
				base_data.angular.z=turningAng/180
				#base_data.angular.z=-(abs(turningAng)/10)**1.2
			elif (turningAng>0):
				base_data.angular.z=turningAng/180
				#base_data.angular.z=(turningAng/10)**1.2
			else:
				base_data.angular.z=0.2
			
			Turning.Angle=base_data.angular.z
		else:
			base_data.angular.z=Turning.Angle
	else:
		Turning.Angle=0
		
		base_data.linear.x=1.5
		turningAng=wil.Straightturn()/100.0
		if (turningAng<0):
			base_data.angular.z=-abs(turningAng)
		elif (turningAng>0):
			base_data.angular.z=abs(turningAng)
	print('turning angle', wil.Straightturn())
	pub.publish(base_data)


if __name__ == '__main__':
    rospy.init_node('reactive_mover_node')
    rospy.Subscriber('base_scan', LaserScan, callback4Laser)
    pub = rospy.Publisher('cmd_vel', Twist, queue_size=100)
    rospy.spin()