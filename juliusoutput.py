
# Import modules
import pexpect # a tool for spawning child applications, controlling them and responding to expected patterns in output
import serial  # serial communication 	
import time    # time related functions
import re      # regular expression matching operations
import sys     # access to variables used or maintained by interpreter	

# configure the serial connection for serial port in eSpeak
ser_espeak = serial.Serial(
    port='/dev/tnt1',  #serial port
    baudrate=115200,
    timeout=2,
)

# configure the serial connection for bluetooth
ser = serial.Serial(
    port='/dev/rfcomm0',  #serial port
    baudrate=115200,
)

ser.isOpen() #open serial port for bluetooth
ser_espeak.isOpen() #open serial port for eSpeak

# a process to extract sentence1, cmscore1 and score1	
def get_confidence(out_text):
    linearray = out_text.split("\n")
    for line in linearray:
        if line.find('sentence1') != -1:
            sentence1 = line
        elif line.find('cmscore1') != -1:
            cmscore1 = line
        elif line.find('score1') != -1:
            score1 = line
    cmscore_array = cmscore1.split()
    err_flag = False
    for score in cmscore_array:
        try:
            ns = float(score)
        except ValueError:
            continue
        if (ns < 0.7): #score chosen by programmer
            err_flag = True
            #print "confidence error:", ns, ":", sentence1
    score1_val = float(score1.split()[1])      
    #if score1_val < -18000: #score1 chosen by programmer
    #    err_flag = True
    #    print "score1 error:", score1_val, sentence1
    if (not err_flag):		#if no errors, print sentence and score and pass a letter serially
        #pass sentence1 to controller functions
	str1 = remove_tags(sentence1)
	str2 = str1.replace('sentence1: ','')
	print "\n Recognised command :", str2
	#print "confidence error:", ns
	process_sentence(sentence1)
    else:
	print "\n Unrecognised Command. Please repeat the command"
        ser.write('U\n') #did not understand
	 
def process_sentence(sentence_text):
	str1 = remove_tags(sentence_text)
	str2 = str1.replace('sentence1: ','')
	motion = ['FORWARD', 'REVERSE']
	rotation=['LEFT', 'RIGHT', 'CLOCKWISE', 'ANTICLOCKWISE']
	stopping = ['STOP', 'HALT', 'BREAK']
	exiting =  ['QUIT', 'EXIT','TERMINATE']
	speed = ['SLOW', 'MEDIUM', 'FAST']
	number = ['TEN', 'THIRTY', 'FORTYFIVE', 'SIXTY', 'EIGHTY']
	transcribed = str2.split()
	x=ser_espeak.read()
	#print (x)
	if x=='/' :

		ser_espeak.flush()
		if transcribed[1] == speed[0] :
			ser.write('G\n') #slow speed 
		elif transcribed[1] == speed[1] :
			ser.write('H\n') #medium speed
		elif transcribed[1] == speed[2] :
			ser.write('I\n') #fast speed
		else:
			ser.write('U\n') #unknown command
	elif x=='*' :
		ser_espeak.flush()
		if transcribed[1] == motion[0] :
			 if transcribed[2] == number[0]:
				ser.write('a\n') #forward10
			 #elif transcribed[0] == number[1]:
			 #	ser.write('b\n') #forward20
			 #	time.sleep(2)
			 elif transcribed[2] == number[1]:
				ser.write('b\n') #forward30
			 elif transcribed[2] == number[2]:
				ser.write('c\n') #forward50
			 elif transcribed[2] == number[3]:
				ser.write('d\n') #forward60
			 #elif transcribed[1] == number[6]:
			 #	ser.write('f\n') #forward70
			 #	time.sleep(5)
			 elif transcribed[2] == number[4]:
				ser.write('e\n') #forward80
		elif transcribed[0] == motion[1] :
			 if transcribed[1] == number[0]:
				ser.write('f\n') #backward10
				#ser_espeak.write('h\n')
			 #elif transcribed[1] == number[1]:
			 #	ser.write('i\n') #backward20
			 #	#ser_espeak.write('h\n')
			 #	time.sleep(5)
			 elif transcribed[1] == number[1]:
				ser.write('g\n') #backward30
			 elif transcribed[1] == number[2]:
				ser.write('h\n') #backward50
			 elif transcribed[1] == number[3]:
				ser.write('i\n') #backward60
			 #elif transcribed[1] == number[6]:
			 #	ser.write('m\n') #backward70
			 #	time.sleep(5)
			 elif transcribed[1] == number[4]:
				ser.write('j\n') #backward80
		elif transcribed[1] == rotation[0]:
			ser.write('o\n') #left
		elif transcribed[1] == rotation[1]:
			ser.write('p\n') #right
		elif transcribed[0] == stopping[0]:
	 			ser.write('z\n') #stop
		elif transcribed[0] == stopping[1]:
	 			ser.write('z\n') #stop
		elif transcribed[0] == stopping[2]:
	 			ser.write('z\n') #stop
		elif transcribed[0] == exiting[0]:
	 			ser.write('y\n') #quit
		elif transcribed[0] == exiting[1]:
	 			ser.write('y\n') #quit
		elif transcribed[0] == exiting[2]:
	 			ser.write('y\n') #quit
		elif transcribed[1] == number[0]:
				if transcribed[3] == rotation[2]:
					ser.write('q\n') #clockwise10
				elif transcribed[3] == rotation[3]:
					ser.write('u\n') #anticlockwise10
		elif transcribed[1] == number[1]:
			 	if transcribed[3] == rotation[2]:
					ser.write('r\n') #clockwise30
				elif transcribed[3] == rotation[3]:
					ser.write('v\n') #anticlockwise30
		elif transcribed[1] == number[2]:
				if transcribed[3] == rotation[2]:
					ser.write('s\n') #clockwise45
				elif transcribed[3] == rotation[3]:
					ser.write('w\n') #anticlockwise45
		elif transcribed[1] == number[3]:
				if transcribed[3] == rotation[2]:
					ser.write('t\n') #clockwise60
				elif transcribed[3] == rotation[3]:
					ser.write('x\n') #anticlockwise60
		else:
			pass
	else:
		pass

# a process to remove unwanted data,
def remove_tags(data):
    p = re.compile(r'<[^<]*?>')
    return p.sub('', data)
    
# a process to determine if an output was generated at all
def process_julius(out_text):
    match_res = re.match(r'(.*)sentence1(\.*)', out_text, re.S)
    if match_res:
        get_confidence(out_text)
	time.sleep(5)
    else:
	#ser.write('B\n')
	pass

# main routine
if __name__ == "__main__":

    print("\n/*--------------------------------------------------------------------*/")
    print("/*---------Speech Recognition Engine (JULIUS) for Khepera 3-----------*/")
    print("/*--------------------------------------------------------------------*/")
    print("/*-----------------------Gabriella Pizzuto----------------------------*/")
    print("/*--------------------------------------------------------------------*/")
    print("/*--------------------Final Year Project 2015-------------------------*/")
    print("/*--------------------------------------------------------------------*/\n")

    #spawning Julius and obtaining output 	
    child = pexpect.spawn ('julius -input mic -C julius.jconf')

    while True:
        try:
            child.expect('please speak')
            process_julius(child.before)
        except KeyboardInterrupt:
            child.close(force=True)
	    print ("\n\n Exiting now ... \n")
	    print(" /*------------------------------GOODBYE-------------------------------*/\n");
            break
			
			
